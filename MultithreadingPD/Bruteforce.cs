﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace MultithreadingPD
{
    class Bruteforce
    {
        public int PinCode { get; set; }
        public static void Main()
        {
            Console.WriteLine("Brute force example");
            Bruteforce bruteforce = new Bruteforce()
            {
                PinCode = 1234,
            };

            Stopwatch watch = Stopwatch.StartNew();
            bruteforce.GeneratePossibilities(9999, 4);
            Console.WriteLine($"Duration: {watch.ElapsedMilliseconds} ms");
        }

        public void GeneratePossibilities(int maxLength, int parts)
        {
            for (int i=1; i <= parts ;i++)
            {
                int maxLengthFraction = (int) Math.Ceiling((float) maxLength / parts);
                int from = maxLengthFraction * (i - 1);
                int to = (maxLengthFraction * i) - 1;
                Console.WriteLine($"from: {from} to: {to}");

                AttempToCrackPinCode(from, to);
            }
        }

        public void AttempToCrackPinCode(int from, int to)
        {
            for (int i=from; i <= to ;i++)
            {
                if (PinCode == i)
                {
                    Console.WriteLine($"The pin code is: {i}");
                }
            }
        }
    }
}
