﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MultithreadingPD
{
    class AsyncHTTPExample
    {
        public static async Task Main()
        {
            string[] urls = {
                "https://www.google.com",
                "https://www.youtube.com",
                "https://www.github.com",
                "https://www.docs.microsoft.com",
                "https://stackoverflow.com",
                "https://sourcemaking.com",
                "https://www.bol.com",
                "https://www.twitch.tv",
                "https://discord.com",
                "https://store.steampowered.com",
            };

            Stopwatch watch1 = Stopwatch.StartNew();
            await GetRequest(urls);
            Console.WriteLine("------------- GetRequest is done -------------");
            long time1 = watch1.ElapsedMilliseconds;

            //Stopwatch watch2 = Stopwatch.StartNew();
            //await GetRequestsWhenAll(urls);
            //Console.WriteLine("----------- GetRequestsWhenAll is done -----------");
            //long time2 = watch2.ElapsedMilliseconds;

            Console.WriteLine($"GetRequest duration: {time1} ms");
            //Console.WriteLine($"GetRequestsWhenAll duration: {time2} ms");
        }

        public static async Task GetRequest(string[] urls)
        {
            using (HttpClient client = new HttpClient())
            {
                foreach (string url in urls)
                {
                    HttpResponseMessage result = await client.GetAsync(url);
                    Console.WriteLine($"status: {result.StatusCode} url: {url}");
                }
            }
        }

        public static async Task GetRequestsWhenAll(string[] urls)
        {

            using (HttpClient client = new HttpClient())
            {
                List<Task> tasks = new List<Task>();

                foreach (string url in urls)
                {
                    tasks.Add(Task.Run(async () =>
                    {
                        HttpResponseMessage result = await client.GetAsync(url);
                        Console.WriteLine($"status: {result.StatusCode} url: {url}");
                    }));
                }

                await Task.WhenAll(tasks);
            }
        }
    }
}
