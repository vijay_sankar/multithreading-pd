﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MultithreadingPD
{
    class DeadlockExample
    {
        private static int _count;
        public static void Main()
        {
            _count = 0;

            Mutex mutex1 = new Mutex();
            Mutex mutex2 = new Mutex();
            DeadlockExample example = new DeadlockExample();

            Thread thread1 = new Thread(new ThreadStart(() => 
            {
                example.Wait1(mutex1, mutex2);
            }));

            Thread thread2 = new Thread(new ThreadStart(() =>
            {
                example.Wait2(mutex2, mutex1);
            }));

            thread1.Start();
            thread2.Start();

            Console.WriteLine("Waiting");
            thread1.Join();
            thread2.Join();

            Console.WriteLine($"Count: {_count}");
            Console.WriteLine("Done");
        }

        public void Wait1(Mutex mutex1, Mutex mutex2)
        {
            mutex1.WaitOne();
            Thread.Sleep(100);

            _count++;
            mutex2.WaitOne();

            mutex1.ReleaseMutex();

            mutex2.ReleaseMutex();
        }

        public void Wait2(Mutex mutex1, Mutex mutex2)
        {
            mutex1.WaitOne();
            Thread.Sleep(100);

            _count++;
            mutex2.WaitOne();

            mutex1.ReleaseMutex();

            mutex2.ReleaseMutex();
        }
    }
}
