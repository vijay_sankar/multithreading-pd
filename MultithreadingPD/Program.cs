﻿using System;
using System.Threading;

namespace MultithreadingPD
{
    class Program
    {
        public static void Main()
        {
            //Thread thread = new Thread(new ThreadStart(Work));
            Thread thread = new Thread(Work);
            thread.Start();
            thread.Join();
            Console.WriteLine("Done");
        }

        public static void Work()
        {
            for (int count = 0; count < 10; count++)
            {
                Console.WriteLine($"Count: {count}");
                Thread.Sleep(100);
            }
        }
    }
}
