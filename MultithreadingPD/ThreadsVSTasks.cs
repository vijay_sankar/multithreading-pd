﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultithreadingPD
{
    class ThreadsVSTasks
    {
        public static void Main()
        {
            Thread thread = new Thread(new ThreadStart(() => 
            {
                Console.WriteLine("Hello world from Thread");
            }));
            thread.Start();
            thread.Join();

            Task task = Task.Run(() => 
            {
                Console.WriteLine("Hello world from Task");
            });
            task.Wait();
        }
    }
}
