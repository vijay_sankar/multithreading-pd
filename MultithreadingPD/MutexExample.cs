﻿using System;
using System.Diagnostics;
using System.Threading;

namespace MultithreadingPD
{
    class MutexExample
    {
        private Mutex _mutex;
        public int Result { get; set; }

        public static void Main()
        {
            MutexExample example = new MutexExample(new Mutex());

            Stopwatch watch = Stopwatch.StartNew();
            example.StartThreads();
            watch.Stop();

            Console.WriteLine($"Result: {example.Result}");
            Console.WriteLine($"Duration: {watch.ElapsedMilliseconds} ms");
        }

        public MutexExample(Mutex mutex)
        {
            _mutex = mutex;
        }

        public void StartThreads()
        {
            Thread[] threads = new Thread[5];
            for (int threadCount = 0; threadCount < 5; threadCount++)
            {
                Thread thread = new Thread(new ThreadStart(Work));
                threads[threadCount] = thread;
                thread.Start();
            }

            foreach (Thread thread in threads)
            {
                thread.Join();
            }
        }

        public void Work()
        {
            _mutex.WaitOne();

            int threadId = Thread.CurrentThread.ManagedThreadId;
            for (int count = 0; count < 5; count++)
            {
                Console.WriteLine($"Thread id: {threadId} count: {count}");
                Thread.Sleep(10);
            }

            Result += 4;
            _mutex.ReleaseMutex();
        }
    }
}
